# Git Essentials

[in development]

1. List of commands you need to know well in order to work well in a team:

    - add
    
    - commit
    
    - log
    
    - stash
    
    - rebase
    
    - merge
    
    - checkout/switch
    
    - branch
    
    - pull
    
    - push
    
    - cherry-pick
    
    - reset
    
    - revert
    
    - config

2. Resolving conflicts

3. Git Flow essentials


